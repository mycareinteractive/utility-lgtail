﻿using Mono.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace LgTail
{
    class Program
    {
        private static bool keepRunning = true;

        private static string comPort = "COM1";
        private static string filter = "** Message: console message:";

        static void Main(string[] args)
        {
            Console.WriteLine("Procentric Web Console Tail Utility - Aceso(C) 2015");
            Console.WriteLine("{0}", typeof(Program).Assembly.GetName());

            // read options using Mono.Options
            if (!ReadOptions(args))
            {
                return;
            }

            Console.WriteLine("              Ctrl-C to stop");
            Console.WriteLine("");

            // open up the serial port and start tailing
            var reader = new SerialReader();
            try
            {
                reader.Open(comPort, filter);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return;
            }

            // wire up ctrl-c event
            Console.CancelKeyPress += delegate(object sender, ConsoleCancelEventArgs e)
            {
                e.Cancel = true;
                Program.keepRunning = false;
                reader.Close();
            };

            while (Program.keepRunning)
            {
                System.Threading.Thread.Sleep(1000);
            }
            Console.WriteLine("exited gracefully");
        }

        static bool ReadOptions(string[] args)
        {
            bool show_help = false;

            var p = new OptionSet() {
                { "p|port=", "the COM port of the serial connection. eg: COM3", 
                   v => comPort = v },
                { "f|filter=", "optional filter string, regex supported",
                   v => filter = v },
                { "h|help",  "show this message and exit", 
                    v => show_help = v != null },
            };

            List<string> extra;
            try
            {
                extra = p.Parse(args);
            }
            catch (OptionException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Try `lgtail --help' for more information.");
                return false;
            }

            if(show_help)
            {
                ShowHelp(p);
                return false;
            }

            return true;
        }

        static void ShowHelp(OptionSet p)
        {
            Console.WriteLine("");
            Console.WriteLine(@"Usage: lgtail [OPTIONS]");
            Console.WriteLine(@"Tail LG procentric debug console for web log.");
            Console.WriteLine(@"If no port is specified, ""COM1"" is used.");
            Console.WriteLine();
            Console.WriteLine(@"Options:");
            p.WriteOptionDescriptions(Console.Out);
        }
    }
}
