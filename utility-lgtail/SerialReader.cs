﻿using OpenNETCF.IO.Serial;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace LgTail
{
    public class SerialReader
    {
        private Port port;
        private DetailedPortSettings portSettings;
        private Int32 bufferSize;
        private string filterString;
        private Regex regex;

        public bool IsConnected
        {
            get;
            private set;
        }

        public SerialReader()
        {
            bufferSize = 4096;
        }

        public void Open(string portName, string filter)
        {
            filterString = filter;
            validatePattern();

            portSettings = new HandshakeXonXoff();
            portSettings.BasicSettings.BaudRate = BaudRates.CBR_115200;
            portSettings.BasicSettings.ByteSize = 8;
            portSettings.BasicSettings.StopBits = StopBits.one;
            portSettings.BasicSettings.Parity = Parity.none;

            // Stupid windows only supports up to "COM9".  For larger number we need to do the domain way "\\.\COM12"
            port = new Port("\\\\.\\" + portName, portSettings);
            port.RThreshold = 1;	// get an event for every 1 byte received
            port.InputLen = bufferSize;		// calling Input will read this number of bytes
            port.SThreshold = 1;	// send 1 byte at a time

            // define an event handler
            port.DataReceived += new Port.CommEvent(port_DataReceived);

            try
            {
                if (port.Open())
                {
                    return;
                }
                else
                {
                    throw new Exception(string.Format("Failed to open port {0}", portName));
                }
            }
            catch(Exception e)
            {
                throw new Exception(string.Format("Failed to open port {0}", portName), e);
            }
        }

        public void Close()
        {
            try
            {
                if (port != null)
                {
                    port.Close();
                    port.Dispose();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                port = null;
            }
        }

        private void port_DataReceived()
        {
            while (port.InBufferCount > 0)
            {
                byte[] inputData = new byte[bufferSize];
                
                // read the character
                inputData = port.Input;

                // display as text
                Encoding enc = Encoding.ASCII;
                String messages = enc.GetString(inputData, 0, inputData.Length);

                // filter
                filterMessage(messages);

            }
        }

        private void filterMessage(string messages)
        {
            var lines = messages.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

            foreach (var l in lines)
            {
                string message = l;
                bool isMatch = false;

                if (regex != null)
                {
                    isMatch = regex.IsMatch(l);
                }
                else
                {
                    isMatch = l.StartsWith(filterString, StringComparison.Ordinal);
                    if (isMatch)
                        message = l.Substring(filterString.Length);
                }

                if (isMatch)
                {
                    Console.WriteLine(message);
                }
            }
        }

        private void validatePattern()
        {
            try
            {
                regex = new Regex(filterString);
            }
            catch
            {
                regex = null;
            }
        }
    }
}
